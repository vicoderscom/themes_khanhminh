<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

require_once(__DIR__.'/.env.php');
// * MySQL settings - You can get this info from your web host * //

define('WP_HOME', $_ENV['WP_HOME']);

define('WP_SITEURL', $_ENV['WP_SITEURL']);

define( 'DISALLOW_FILE_MODS', true );

/* The name of the database for WordPress */
//define( 'WP_CACHE', true );

define('DB_NAME', $_ENV['DB_NAME']);

/* MySQL database username */
define('DB_USER', $_ENV['DB_USER']);

/* MySQL database password */
define('DB_PASSWORD', $_ENV['DB_PASSWORD']);

/* MySQL hostname */
define('DB_HOST', $_ENV['DB_HOST']);

/* Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/* The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD','direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WFAQtC^R;5Ex-1|rPj|M@5;EIY]LDZ3-pw+QzD--BD<qUPVGDO-00ay8-&7n(_P`');
define('SECURE_AUTH_KEY',  'E?|Q=B+LN3=t.^1%sm~9*ttoP*_W(Lb*Wf2@F7SHU %HIbUxEJy.EjIp!BsY]T$2');
define('LOGGED_IN_KEY',    'qA3c(.dt&A^.}-`7)~yW1qP#X%-rrc|Cuyjf#QsZezidL#e+B6z0Bn/7k+M4oeZ2');
define('NONCE_KEY',        '+h0A-]+pk^nqk9[pVG<ON?M-} @38.=rGI.,C<*![wa)J09PSlu*HrW++~4TOyq.');
define('AUTH_SALT',        '`uH+ii>Mhz-/&B8xb~IjP 5I6XBh4f, ]R6/_ff/;NU/r(Uu[yKfJ65V>N:jha#/');
define('SECURE_AUTH_SALT', '9M:]cUB#tJp-[US&a_VJQZjyN76MSv7=cPIYe:oL=i--ZX]<#W+<@4`m1Jj9O|<6');
define('LOGGED_IN_SALT',   'k}5~/g8%-|uQU*IDTNHe{5>_-5T<V BCX-%XnWa8Cm0-+u|Yf!LcJ4-?|-FELLrn');
define('NONCE_SALT',       'Av[fW*/S$p$Zg>f8S=I!&~AzBTf~vct-`)Oyu@Y=]6h`<q0QK,0:DuvX4D+4.KLX');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
 define('ABSPATH', dirname(__FILE__) . '/');

/* Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');