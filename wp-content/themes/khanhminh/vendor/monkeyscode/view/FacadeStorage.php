<?php
namespace MSC;

use Illuminate\Support\Facades\Facade;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

class FacadeStorage extends Facade
{
    /**
     * [getFacadeAccessor description]
     * 
     * @since  1.1.0
     * @return [type] [description]
     */
    protected static function getFacadeAccessor()
    {
    	$viewPath = dirname(dirname(dirname(__DIR__)));
        $adapter = new Local($viewPath);
        $filesystem = new Filesystem($adapter);
        return $filesystem;
    }
}
