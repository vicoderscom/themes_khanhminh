<?php
namespace MSC;

use Philo\Blade\Blade;
use MSC\FacadeStorage;

class View
{
    /**
     * view path
     * @var string
     */
    public $viewPath;

    /**
     * cache path
     * @var string
     */
    public $cachePath;

    /**
     * root path for filesystem
     * @var string
     * @since 1.1.1
     */
    protected $rootPath;

    /**
     * a partial of view path
     * @var string
     * @since 1.1.1
     */
    protected $viewDir;

    /**
     * a partial of cache path
     * @var string
     * @since 1.1.1
     */
    protected $cacheDir;

    /**
     * constructor
     *
     */
    public function __construct()
    {
        $this->setRootPath();
        $this->setViewDir(DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'views');
        $this->setCacheDir(DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'cache');
        $this->setViewPath($this->rootPath . $this->viewDir);
        $this->setCachePath($this->rootPath . $this->cacheDir);
        $this->createCacheDir();
    }

    /**
     * return blade template
     *
     * @param  string $view view name
     * @param  array  $data data pass to view
     * @return string
     */
    public function render($view, $data = [])
    {
        $this->writeFile($view);
        $blade = new Blade($this->getViewPath(), $this->getCachePath());

        if (is_array($data)) {
            return $blade->view()->make($view, $data)->render();
        } else {
            throw new Exception('data pass into view must be an array.');
        }
    }

    /**
     * create blade file
     * 
     * @param  string $path path to blade file
     * @since  1.1.0
     * @return boolean
     */
    public function writeFile($path)
    {
        $fullPath = $this->viewDir . DIRECTORY_SEPARATOR . $this->transformDotToSlash($path) . '.blade.php';

        if (!FacadeStorage::has($fullPath)) {
            FacadeStorage::createDir(dirname($fullPath));
            return FacadeStorage::write($fullPath, '');
        }

        return false;
    }

    /**
     * create cache folder
     * 
     * @since 1.1.1
     */
    public function createCacheDir()
    {
        if (!FacadeStorage::has($this->cacheDir)) {
            return FacadeStorage::createDir($this->cacheDir);
        }

        return false;
    }

    /**
     * convert dot path to slash path
     * 
     * @param  string $path path to blade file
     * @since  1.1.0
     * @return boolean
     */
    public function transformDotToSlash($path)
    {
        return str_replace('.', '/', $path);
    }

    /**
     * set root path
     * 
     * @since  1.1.0
     * @return void
     */
    public function setRootPath()
    {
        $this->rootPath = dirname(dirname(dirname(__DIR__)));
    }

    /**
     * set path to views folder
     * 
     * @param  string $viewPath
     * @return void
     */
    public function setViewPath($viewPath)
    {
        $this->viewPath = $viewPath;
    }

    public function setCachePath($cachePath)
    {
        $this->cachePath = $cachePath;
    }

    public function setViewDir($viewDir)
    {
        $this->viewDir = $viewDir;
    }

    public function setCacheDir($cacheDir)
    {
        $this->cacheDir = $cacheDir;
    }

    /**
     * [getViewPath description]
     * @return [type] [description]
     */
    public function getViewPath()
    {
        return $this->viewPath;
    }

    /**
     * [getCachePath description]
     * @return [type] [description]
     */
    public function getCachePath()
    {
        return $this->cachePath;
    }
}
