<?php
namespace MSC;

use Illuminate\Support\Facades\Facade;

class FacadeView extends Facade
{
    /**
     * [getFacadeAccessor description]
     * @return [type] [description]
     */
    protected static function getFacadeAccessor()
    {
        return new \MSC\View;
    }
}
