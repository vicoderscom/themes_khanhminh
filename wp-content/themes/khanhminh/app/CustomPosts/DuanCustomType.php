<?php
/**
 * This file create Duan custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class DuanCustomType extends CustomPost
{
    /**
     * [$type description]
     * @var  string
     */
    public $type = 'duan';

    /**
     * [$single description]
     * @var  string
     */
    public $single = 'Dự án';

    /**
     * [$plural description]
     * @var  string
     */
    public $plural = 'Dự án';

    /**
     * $args optional
     * @var  array
     */
    public $args = ['menu_icon' => 'dashicons-location-alt'];

}
