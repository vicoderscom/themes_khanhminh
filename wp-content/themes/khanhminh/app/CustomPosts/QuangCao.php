<?php
/**
 * Sample class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class QuangCao extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'quangcao';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'QuangCao';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'QuangCao';

    /**
     * $args optional
     * @var array
     */
    public $args = ['menu_icon' => 'dashicons-art'];

}
