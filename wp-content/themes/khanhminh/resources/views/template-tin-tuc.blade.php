@extends('layouts.full-width')


@section('content')

		@include('partials.page-header')
				
		<div class="category">
			<div class="container">
				<div class="row tintuc">
		        	<div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 category-news">
		        		<div class="category-content">
							<div class="cat-title">
								<h3><?php echo "Dự án" ?></h3>
							</div>
							    @php
								    $shortcode = '[listing post_type="duan" layout="partials.content-tin-tuc" per_page = "4"]';
								    echo do_shortcode($shortcode);
							    @endphp
				        </div>
                        <div class="intro-home-button">
                            <a href="{{ get_post_type_archive_link('duan') }}">
                                <?php _e('See more','khanhminh'); ?>
                            </a>
                        </div>
				    </div>
				    {{ view('sidebar') }}
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 category-news" >
                	<div class="category-content">
                    
                    <div class="cat-title">
                        <h3>
                            Tin tức mới nhất
                        </h3>
                    </div>
						    @php
						    	$shortcode = '[listing post_type="post" per_page="4" layout="partials.content-tin-tuc"]';
						    	echo do_shortcode($shortcode);
						    @endphp

                        <div class="intro-home-button">
                            <a href="{{ get_page_link(248) }}">
                                <?php _e('See more','khanhminh'); ?>
                            </a>
                        </div>
                    </div>

                </div>
				    
		        </div>
		    </div>
		</div>
@endsection
