<!DOCTYPE html>

<html {!! language_attributes() !!}>

  @include('partials.head')
    <style type="text/css" id="custom-background-css">
    body.custom-background { background-color: #bdd96e; }
    </style>

    <body {!! body_class() !!}>

    {!! do_action('get_header') !!}

    @include('partials.header')

    <div class="wrap fluid-container" role="document">

        @yield('banner')

        <div class="content">
            <main class="main">
                @yield('content')
            </main>
        </div>
        
    </div>

    {!! do_action('get_footer') !!}

    @include('partials.footer')

    {!! wp_footer() !!}

    </body>
    
</html>
