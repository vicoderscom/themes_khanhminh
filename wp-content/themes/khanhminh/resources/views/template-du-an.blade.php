@extends('layouts.full-width')


@section('content')
    @while(have_posts())

        {!! the_post() !!}

        @include('partials.page-header')

        <div class="duan">
            @php
                $shortcode = '[listing post_type="duan" layout="partials.content-du-an" paged="yes" per_page="2"]';
                echo do_shortcode($shortcode);
            @endphp
        </div>

    @endwhile

    {!! get_the_posts_navigation() !!}
@endsection


