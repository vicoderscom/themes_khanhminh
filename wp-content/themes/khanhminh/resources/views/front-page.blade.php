@extends('layouts.full-width')

@section('content')

    @while (have_posts())

        {!! the_post() !!}

<div class="container">
    <div class="row">
        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 frontpage-left">
        <div class="banner-home" data-aos="fade-up" data-aos-duration="1000">
            @php
                $shortcode = '[listing post_type="bannerhome" layout="partials.content-front-page"]';
                echo do_shortcode($shortcode);
            @endphp
        </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 project-news product-news">

                    @php
                        $product_news_home = get_option('box_news_home_customize_product_news_home');
                    @endphp
                    
                    <div class="cat-title">
                        <h3>
                            {{ get_term($product_news_home)->name }}
                        </h3>
                    </div>

                    @php
                        $shortcode = '[listing post_type="duan" taxonomy="duan-category('.$product_news_home.')" per_page=1 layout="partials.frontpage-project-news"]';
                        echo do_shortcode($shortcode);
                    @endphp
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 project-news">

                    @php
                        $project_news_home = get_option('box_news_home_customize_project_news_home');
                    @endphp

                    <div class="cat-title">
                        <h3>
                            {{ get_term($project_news_home)->name }}
                        </h3>
                    </div>

                    @php
                        $shortcode = '[listing post_type="duan" taxonomy="duan-category('.$project_news_home.')" per_page=1 layout="partials.frontpage-project-news"]';
                        echo do_shortcode($shortcode);
                    @endphp
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 intro-home">
                    
                    <div class="cat-title">
                        <h3>
                            {{ get_field('tieu_de') }}
                        </h3>
                    </div>

                    <div class="intro-home-content">
                        {!! get_field('noi_dung') !!}
                        
                        <div class="intro-home-button">
                            <a href="{{ get_field('duong_linh') }}">
                                <?php _e('See more','khanhminh'); ?>
                            </a>
                        </div>
                    </div>

                </div>

            </div>            
        </div>
        {{ view('sidebar') }}
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 intro-home new_post" >
                    
                    <div class="cat-title">
                        <h3>
                            Tin tức mới nhất
                        </h3>
                    </div>

                    <div class="intro-home-content row" id="new_post">
                        <?php 
                            $post = array(
                                'post_type'=>'post',
                                'posts_per_page'=> 8

                            );
                            if($post['post_type']=='post'){
                             add_action('pre_get_posts', function ($query) {
                                $query->set('posts_per_page', 8);
                              });
                            }

                            $newPost = new WP_Query($post);
                         ?>
                        <?php while ($newPost->have_posts()) : $newPost->the_post(); ?>
                           {{ view('partials.content-new-post') }}
                        <?php endwhile ; wp_reset_query() ;?>
                    </div>
                        <div class="intro-home-button">
                            <a href="{{ get_page_link(248) }}">
                                <?php _e('See more','khanhminh'); ?>
                            </a>
                        </div>

                </div>

    </div>
</div>
        
    @endwhile

    
@endsection