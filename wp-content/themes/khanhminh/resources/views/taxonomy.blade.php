@extends('layouts.full-width')

@php
    $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $term_id = $term->term_id;
    $term_name = $term->name;
@endphp

@section('banner')

    @php
        $image_tax = get_field('image_tax','duan-category_'.$term_id.'');
        $banner_img_check = $image_tax['url'];
    @endphp

    <div class="banner-no-home">

        @if (!empty($banner_img_check))
            <img src="{{ $banner_img_check }}">
        @else
            <img src="{{ asset2('images/banner-trang-trong.jpg') }}">
        @endif

    </div>

@endsection

@section('content')

    <div class="page-header">
        <h1>{{ $term_name }}</h1>
    </div>

    <div class="duan">
        @php
            $shortcode = '[listing post_type="duan" taxonomy="duan-category('.$term_id.')" layout="partials.content-du-an"  paged="yes" per_page="2"]';
            echo do_shortcode($shortcode);
        @endphp
    </div>

    {!! get_the_posts_navigation() !!}
@endsection


