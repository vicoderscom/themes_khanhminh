@extends('layouts.full-width')


@section('content')
    @while(have_posts())

        {!! the_post() !!}

        @include('partials.page-header')

        <div class="chuyenvien">
        	<div class="container">
                
                <div class="chuyenvien-meta-title">
                    {{ get_field('ten_doi_ngu') }}
                </div>

	            @php
	                $shortcode = '[listing post_type="chuyenvien" layout="partials.content-chuyen-vien" paged="yes" per_page="4"]';
	                echo do_shortcode($shortcode);
	            @endphp
			</div>
		</div>

    @endwhile

    {!! get_the_posts_navigation() !!}
@endsection


