@extends('layouts.full-width')


@section('content')
    @while(have_posts())

        {!! the_post() !!}

		@include('partials.page-header')
				
		<div class="giai-phap">
			<div class="giai-phap-meta">
				<div class="container">
					{!! get_field('mo_ta_giai_phap') !!}
				</div>
			</div>
			<div class="container">
				<div class="giai-phap-content">
		        	{!! wpautop(the_content()) !!}
		    	</div>
		    </div>
		</div>
    @endwhile        
@endsection


