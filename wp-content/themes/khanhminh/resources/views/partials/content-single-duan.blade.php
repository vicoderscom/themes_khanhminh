<div>

    <div class="duan-content single-duan">
        <div class="container">
            <div class="single-content">
                <div>
                    <h1 class="entry-title">{{ get_the_title() }}</h1>
                </div>

                <article class="item">

                    {{ view('partials.slides-duan') }}

                </article>

                <div class="single-duan-info">

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">
                                <?php _e('Overview project','khanhminh'); ?>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">
                                <?php _e('Information details','khanhminh'); ?>
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="profile">
                            <ul class="content-field-duan">
                                <?php
                                    $tong_quan_du_an = get_field('tong_quan_du_an');
                                    foreach ($tong_quan_du_an as $tqda) {
                                        echo '
                                            <li><span class="name-field-duan">'.$tqda['ten_truong'].'</span>
                                                <span class="info-field-duan">'.$tqda['thong_tin_truong'].'</span>
                                            </li>';
                                    }
                                ?>
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="buzz">
                            {!! get_field('thong_tin_chi_tiet') !!}
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
                    
</div>
