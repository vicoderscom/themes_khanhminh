<footer class="footer fluid-container">
    <div class="row" style="margin-left: 0px;">
    <div class="widget footer-top col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3">
        @php
            if(is_active_sidebar('footer3')){
                dynamic_sidebar('footer3');
            }
        @endphp
    </div>
    <div class="widget footer-top col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <div class="footer_post">
            <div class="title_footer_post">
                Bài Viết Nổi bật
            </div>
            <div class="content">
                    @php
                        $post = array(
                            'post_type'=>'post'
                        );
                                    if($post['post_type']=='post'){
                                     add_action('pre_get_posts', function ($query) {
                                        $query->set('posts_per_page', -1);
                                      });
                                    }
                        $query = new WP_Query($post);
                        while($query->have_posts()) : $query->the_post();

                        $field = get_field( "new_footer", get_the_ID() );
                        
                        if($field[0] === 'co'){
                    @endphp
                    <div class="row footer_item">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 col-12 footer_item_img">
                            @php
                                if(has_post_thumbnail()){
                                    the_post_thumbnail();
                                }
                            @endphp
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6 col-xs-12 col-12 footer_item_title">
                            <a href="{{ get_permalink() }}" title="{{ get_the_title() }}">
                                {{ the_title() }}
                            </a>
                        </div>
                    </div>
                    
                    @php
                        }
                        endwhile;
                    @endphp
                
            </div>
        </div>
    </div>
     <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 widget footer-top ">
        <div class="footer_post">
            <div class="title_footer_post">
                Dự án
            </div>
            <div class="content">
                    @php
                        $post = array(
                            'post_type'=>'duan'
                        );
                                    if($post['post_type']=='duan'){
                                     add_action('pre_get_posts', function ($query) {
                                        $query->set('posts_per_page', 6);
                                      });
                                    }
                        $query = new WP_Query($post);
                        while($query->have_posts()) : $query->the_post();
                    @endphp
                    <div class="row footer_item">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 footer_item_duan">
                            <a href="{{ get_permalink() }}" title="{{ get_the_title() }}">
                                <i class="fa fa-plus" aria-hidden="true"></i>{{ the_title() }}
                            </a>
                        </div>
                    </div>
                    @endwhile
                
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 widget footer-top " id="footer_ad">
        @if (ICL_LANGUAGE_CODE == 'vi')
            <div class="title_footer_post">{!! get_option('footer_customize_title_vn') !!}</div>
            {!! get_option('footer_customize_address_vn') !!}
        @else
            <div class="title_footer_post">{!! get_option('footer_customize_title_en') !!}</div>
            {!! get_option('footer_customize_address_en') !!}
        @endif
    </div>
    </div>
    <div class="footer-bottom">
        @if (ICL_LANGUAGE_CODE == 'vi')
            {{ get_option('footer_customize_copyright_vn') }}
        @else
            {{ get_option('footer_customize_copyright_en') }}
        @endif
    </div>
</footer>

<div id="back-to-top">
    <a href="javascript:void(0)">
        <i class="fa fa-chevron-up" aria-hidden="true"></i>
    </a>
</div>
