<div class="item col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3">
	<div class="img-new-post">
		<?php if(has_post_thumbnail()){
			the_post_thumbnail();
		} ?>
	</div>
	<div class="title-new-post">
		<a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>">
			<?php echo the_title(); ?>
		</a>
	</div>
	<div class="des-new-post">
		<?php echo createExcerptFromContent(get_the_excerpt(),35); ?>
	</div>
	{{ view('partials.entry-see-details') }}
</div>