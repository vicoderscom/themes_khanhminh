<article class="item">

    <img src="{{ asset2('images/3x1.png') }}" alt="{{ $title }}" style="background-image: url({{ $thumbnail }});" />

    <div class="banner-title">
        <a href="{{ get_the_excerpt() }}">
        	<h3>{{ $title }}</h3>
        </a>
    </div>

</article>


