@php
    $duan = get_field("tong_quan_du_an", get_the_ID() );
@endphp
    <div class="row">
            <article class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 col-12">
                <figure>
                    <a href="{{ get_the_permalink() }}" title="{{ get_the_title() }}">
                        <img src="{{ asset2('images/3x2.png') }}" alt="{{ get_the_title() }}" style="background-image: url({{ getPostImage(get_the_ID(), 'du-an') }});" />
                    </a>
                </figure>
            </article>
            
            <article class="col-lg-6 col-xl-6  col-md-6 col-sm-6 col-xs-12 col-12 content-du-an">
                <ul>
                    @php
                        foreach($duan as $duan){
                            echo "<li>";
                                echo "<span>".$duan["ten_truong"].": </span>".$duan["thong_tin_truong"]."<br>";
                            echo "</li>";
                        }
                    @endphp
                </ul>
            </article>
    </div>