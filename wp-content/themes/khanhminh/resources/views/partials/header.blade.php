<header class="header">
    <div class="fuild-container">
        <div class="header-content">
            <div class="row headertop">
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2 logo">
                    <figure>
                        <a href="{!! icl_get_home_url() !!}">
                            <img src="{{ get_option('header_customize_logo') }}">
                        </a>
                    </figure>
                </div>
                <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10-col-xs-10 header-top">
                    
                    @if (ICL_LANGUAGE_CODE == 'vi')
                        {{ get_option('header_customize_slogan_vn') }}
                        <div class="address"><i class="fa fa-map-marker" aria-hidden="true"></i>
                            {{ get_option('header_customize_address_vi') }}
                        
                        </div>
                    @else
                        {{ get_option('header_customize_slogan_en') }}
                    @endif
                </div>   
            </div>
            <div class="header-right">
                <div class="header-bottom">
                    <nav class="menu">
                        <div class="main-menu">
                            @if (has_nav_menu('main-menu'))
                                {!! wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'menu-primary']) !!}
                            @endif
                        </div>
                        <div class="mobile-menu"></div>
                    </nav>                   
                    <div class="header-bottom-right">
                        <div class="search-box">
                            <form action="{!! esc_url( home_url( '/' ) ) !!}">
                                <input type="text" placeholder="" required requiredmsg="
                                    <?php
                                        if (ICL_LANGUAGE_CODE == 'vi') {
                                            echo 'Bạn vui lòng không để trống ˚¬˚ ';
                                        } else {
                                            echo 'You no empty now ˚¬˚ ';
                                        }
                                    ?>" id="search-box" name="s" value="{!! get_search_query() !!}">
                                <div class="search-icon">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </div>
                            </form>
                        </div>
                        <div class="language">
                            {!! do_action('wpml_add_language_selector') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>




