
    <div class="row">
        @php
            $imgduans = get_field("anh_du_an", get_the_ID() );
            foreach ($imgduans as $imgduan){
        @endphp
            <article class="col-12">
                <figure>
                    <a href="{{ get_the_permalink() }}" title="{{ get_the_title() }}">
                        <img src="{{ asset2('images/3x2.png') }}" alt="{{ get_the_title() }}" style="background-image: url({{ $imgduan["url"] }});" />
                    </a>
                </figure>
            </article>
        @php
            }
        @endphp
    </div>