@extends('layouts.full-width')


@section('content')
    @while(have_posts())

		{!! the_post() !!}

        @include('partials.page-header')

        <div class="gioi-thieu">
	        <div class="container">

	        	<div class="single-content gioi-thieu-content">
    				{!! wpautop(the_content()) !!}
		        </div>

		        <div class="gioi-thieu-video">
		        	{{ the_field('video') }}
		    	</div>
				    
			</div>
		</div>
        
    @endwhile
@endsection
