@extends('layouts.full-width')


@section('content')
    @while(have_posts())

		{!! the_post() !!}

        @include('partials.page-header')
        
        <div class="contact">
	        <div class="container">
	        	<div class="row">
	        		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 contact-form-contact">
	        			{!! wpautop(the_content()) !!}
				        
				        @php 
	                        dynamic_sidebar('contact-form');
	                    @endphp
				    </div>
				    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 contact-map">
				    	@php 
	                        dynamic_sidebar('contact-map');
	                    @endphp
				    </div>
				</div>
			</div>
		</div>
        
    @endwhile
@endsection
