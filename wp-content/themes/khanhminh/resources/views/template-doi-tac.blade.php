@extends('layouts.full-width')


@section('content')
    @while(have_posts())

        {!! the_post() !!}

        @include('partials.page-header')

        <div class="doitac">
        	<div class="container">
        		<div class="row">
		            @php
		                $shortcode = '[listing post_type="doitac" layout="partials.content-doi-tac" paged="yes" per_page="6"]';
		                echo do_shortcode($shortcode);
		            @endphp
			    </div>
			</div>
		</div>

    @endwhile

    {!! get_the_posts_navigation() !!}
@endsection


